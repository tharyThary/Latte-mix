#Latte-mix components installer v0.1 (LCI)

import os

def setup():
    print("Latte-mix components installer v0.1")
    setup = input("Choose a Linux distribution:\n1.Debian/Ubuntu/Linux Mint/LMDE\n2.Fedora/RHEL\n3.OpenSUSE\n>>>")

    if setup == "1":
        print("""
Distibution:
______     _     _             
|  _  \   | |   (_)            
| | | |___| |__  _  __ _ _ __  
| | | / _ \ '_ \| |/ _` | '_ \ 
| |/ /  __/ |_) | | (_| | | | |
|___/ \___|_.__/|_|\__,_|_| |_|

Package Manager: apt
Required distribution version:
-> Debian: 10, 11, Testing, Sid
-> Ubuntu: 21.04, 21.10, 22.04
-> Linux Mint: 20, 20.1, 20.2, 20.3, 21
-> LMDE: 4, 5
-> Devuan: 4
              """)
        inst = input("Install components? Y/n:")
        if inst == "Y":
            os.system('sudo apt install openbox tint2 thunar rofi adapta-gtk-theme feh obconf xorg terminator sxhkd plank')

        if inst == "n":
            quit()



    if setup == "2":
         print("""
Distibution:
______ _   _  _____ _     
| ___ \ | | ||  ___| |    
| |_/ / |_| || |__ | |    
|    /|  _  ||  __|| |    
| |\ \| | | || |___| |____
\_| \_\_| |_/\____/\_____/
                          
                          
Package Manager: dnf, yum
Required distribution version:
-> RHEL: 8, 9
-> Fedora: 34, 35, 36
              """)
         inst = input("Install components? Y/n:")
         if inst == "Y":
            os.system('sudo dnf install openbox tint2 thunar rofi adapta-gtk-theme feh obconf xorg-x11-server-Xorg terminator sxhkd plank')

         if inst == "n":
             quit()
             
    if setup =="3":
        print("""
Distibution:
 _____                  _____ _   _ _____ _____ 
|  _  |                /  ___| | | /  ___|  ___|
| | | |_ __   ___ _ __ \ `--.| | | \ `--.| |__  
| | | | '_ \ / _ \ '_ \ `--. \ | | |`--. \  __| 
\ \_/ / |_) |  __/ | | /\__/ / |_| /\__/ / |___ 
 \___/| .__/ \___|_| |_\____/ \___/\____/\____/ 
      | |                                       
      |_|                                       
Package Manager: zypper
Required distribution version:
-> OpenSUSE: 15.3, 15.4
-> SLES: 11, 12, 15
              """)
        inst = input("Install components? Y/n:")
        if inst == "Y":
            os.system('sudo zypper install openbox tint2 thunar rofi adapta-gtk-theme feh obconf xorg-x11-server terminator sxhkd plank')

        if inst == "n":
             quit()
        

setup()        
